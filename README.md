# ~/ABRÜPT/JOSEPHINE LANESEM/TRESORS ET TROUVAILLES/*

La [page de ce livre](https://abrupt.ch/josephine-lanesem/tresors-et-trouvailles/) sur le réseau.

## Sur le livre

Cheminement parmi des collections classiques ou insolites – de voix, de nœuds, d’éponges, d’origami… – qui racontent autant l’objet collectionné que le sujet collectionneur. La trouvaille devient trésor, la matière inerte révèle sa magie muette, la vie s’enchante, discrètement, de joies mineures. Manière de créer un monde dans le monde, à sa mesure. D’esquiver le non-sens par une curiosité renouvelée. D’oublier la duplicité des mots et la disparition des êtres dans la persistance modeste des choses. Les objets restent. On peut leur faire confiance. Passeurs entre les vivants et les morts, ils figurent le lien qui vient à manquer. Mais si on leur accorde trop de place, ils commencent à s’animer d’une vie propre…

## Sur l'autrice

Joséphine Lanesem est née en 1988 à Paris. Elle y a grandi et étudié la philosophie et l'histoire de l'art, avant de partir vivre à Cagliari, Lisbonne, Berlin et maintenant Barcelone. Un jour, elle parle italien, l'autre français, et la langue des rêves, elle l'écrit.

[Nervures et Entailles](https://josephinelanesem.com/), le site de Joséphine Lanesem.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
