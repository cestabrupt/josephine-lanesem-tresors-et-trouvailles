var el = document.getElementById('antilivre');
var sortable = Sortable.create(el,{
  animation: 500,
  draggable: ".mouvement",
  fallbackTolerance: 3,
});

document.querySelectorAll('.conteneur p').forEach(el => el.classList.add('none'));

var conteneurs = document.querySelectorAll('.conteneur');
for (let i = 0; i < conteneurs.length; i++) {
  conteneurs[i].onclick = function() {
    var c = 0;
    while (c < conteneurs.length) {
      conteneurs[c++].classList.add('none');
    }
    this.classList.remove('none');
    this.classList.remove('mouvement');
    this.classList.add('plein');
    this.querySelectorAll('p').forEach(el => el.classList.remove('none'));
    document.querySelector('.informations').classList.add('informations--contenu');
    document.querySelector('.informations').classList.toggle('informations--croix');
  };
}


var informations = document.querySelector('.informations');

informations.addEventListener('click', function(e) {

  if (informations.classList.contains('informations--contenu')) {
    document.querySelector('.plein').classList.add('mouvement');
    document.querySelector('.plein').classList.remove('plein');
    document.querySelectorAll('.conteneur').forEach(el => el.classList.remove('none'));
    document.querySelectorAll('.conteneur p').forEach(el => el.classList.add('none'));
    document.querySelector('.informations').classList.remove('informations--contenu');

  } else if (informations.classList.contains('informations--titre')) {
    document.querySelectorAll('.page-titre, .conteneur').forEach(el => el.classList.toggle('none'));
    document.querySelector('.informations').classList.remove('informations--titre');

  } else {
    document.querySelector('.informations').classList.add('informations--titre');
    document.querySelectorAll('.page-titre, .conteneur').forEach(el => el.classList.toggle('none'));
  };

    document.querySelector('.informations').classList.toggle('informations--croix');
});
